<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home page
 */
class Home extends MY_Controller {

	public function index()
	{
		//ada perubahan
		$this->render('home', 'full_width');
	}
}
