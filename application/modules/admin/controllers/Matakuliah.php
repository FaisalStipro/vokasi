<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Panel management, includes:
 * 	- Admin Users CRUD
 * 	- Admin User Groups CRUD
 * 	- Admin User Reset Password
 * 	- Account Settings (for login user)
 */
class Matakuliah extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Admin Users CRUD
	public function index()
	{
		$crud = $this->generate_crud('tbmatakuliah');
		$crud->columns('matakuliah', 'sks');
		$this->unset_crud_fields('ip_address', 'last_login');

		// cannot change Admin User groups once created
	//	if ($crud->getState()=='list')
		//{
		//                     //feld di tbmhs  ....
	//		$crud->set_relation('jurusan', 'tb_jurusan', 'nama_jur');
	//	}



		// disable direct create / delete Admin User
///		$crud->unset_add();
//		$crud->unset_delete();

		$this->mPageTitle = 'FORM MATAKULIAH';
		$this->render_crud();
	}


	public function create()
	{
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$nim = $this->input->post('nim');
			$jurusan = $this->input->post('jurusan');
			$nama = $this->input->post('nama');
			$tlp = $this->input->post('tlp');


			// proceed to create user
			$user_id = $this->ion_auth->register($nim, $jurusan, $nama, $tlp);
			if ($user_id)
			{
				// success
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);

				// directly activate user
				$this->ion_auth->activate($user_id);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		// get list of Frontend user groups
		$this->mPageTitle = 'Input Data Mahasiswa ';

		$this->mViewData['form'] = $form;
		$this->render('mahasiswa/create');
	}



}
