<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Panel management, includes:
 * 	- Admin Users CRUD
 * 	- Admin User Groups CRUD
 * 	- Admin User Reset Password
 * 	- Account Settings (for login user)
 */
class Jurusan extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Admin Users CRUD
	public function index()
	{
		$crud = $this->generate_crud('tb_jurusan');
		$crud->columns('nama_jur', 'jenjang');
		$this->unset_crud_fields('ip_address', 'last_login');

		// cannot change Admin User groups once created
	//	if ($crud->getState()=='list')
		//{
		//                     //feld di tbmhs  ....
	//		$crud->set_relation('jurusan', 'tb_jurusan', 'nama_jur');
	//	}



		// disable direct create / delete Admin User
///		$crud->unset_add();
//		$crud->unset_delete();

		$this->mPageTitle = 'FORM Jurusan';
		$this->render_crud();
	}



}
