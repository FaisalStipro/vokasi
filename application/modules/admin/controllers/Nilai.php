<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin Panel management, includes:
 * 	- Admin Users CRUD
 * 	- Admin User Groups CRUD
 * 	- Admin User Reset Password
 * 	- Account Settings (for login user)
 */
class Nilai extends Admin_Controller {

	public function __construct()
	{

		parent::__construct();
		$this->load->library('form_builder');
	//	$form = $this->form_builder->create_form();

	}

	// Admin Users CRUD
	public function index()
	{
		$crud = $this->generate_crud('tbnilai');
		$crud->columns('nim', 'matakuliah','nilai','ipk');
		$this->unset_crud_fields('ip_address', 'last_login');

		// cannot change Admin User groups once created
	//	if ($crud->getState()=='list')
		//{
		//                     //feld di tbmhs  ....
		$crud->set_relation('nim', 'tb_mhs', 'nim');
		$crud->set_relation('matakuliah', 'tbmatakuliah', 'matakuliah');
//	}



		// disable direct create / delete Admin User
///		$crud->unset_add();
//		$crud->unset_delete();

		$this->mPageTitle = 'FORM Nilai Mahasiswa';
		$this->render_crud();
	}


	public function datanilai()
	{

$this->load->model('modelnilai');
//$form = $this->form_builder->create_form();

$data = $this->modelnilai->getNilai();
$this->mViewData['data']=$data;
//$this->mViewData['form']=$form;
$this->render('nilai/datanilai');
	}



}
