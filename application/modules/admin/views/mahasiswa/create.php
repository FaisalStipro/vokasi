<?php echo $form->messages(); ?>

<div class="row">

	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Informasi Data Mahasiswa</h3>
			</div>
			<div class="box-body">
				<?php echo $form->open(); ?>

					<?php echo $form->bs3_text('Nim', 'nim'); ?>
					<?php echo $form->bs3_text('Nama', 'nama'); ?>
					<?php echo $form->bs3_text('Jurusan', 'jurusan'); ?>
					<?php echo $form->bs3_text('Telphone', 'tlp'); ?>




					<?php echo $form->bs3_submit(); ?>

				<?php echo $form->close(); ?>
			</div>
		</div>
	</div>

</div>
